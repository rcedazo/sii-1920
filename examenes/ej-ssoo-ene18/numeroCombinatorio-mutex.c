// Compilar con opcion: -lpthread

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <semaphore.h>

pthread_mutex_t mutex;
pthread_cond_t espera;
int x=1, y=1, n=7, k=4;
int hayNumerador=0; // Falso al principio

void Proceso1() {
	int i;
	for (i=n-k+1; i<=n; i++) {
		x=x*i;
	}
	pthread_mutex_lock(&mutex);
	hayNumerador=1;
	pthread_cond_signal(&espera);
	pthread_mutex_unlock(&mutex);
}

void Proceso2() {
	int i;
	for (i=2; i<=k; i++) {
		y=y*i;		
	}
	pthread_mutex_lock(&mutex);
	while(hayNumerador == 0) {
		pthread_cond_wait(&espera, &mutex);
	}
	printf("C(n,k) = %d\n", x/y);
	pthread_mutex_unlock(&mutex);
}

int main() {
	pthread_t th1, th2;

	pthread_mutex_init(&mutex, NULL);
	pthread_cond_init(&espera, NULL);

	pthread_create(&th1, NULL, Proceso1, NULL);
	pthread_create(&th2, NULL, Proceso2, NULL);
	pthread_join(th1, NULL);
	pthread_join(th2, NULL);

	pthread_mutex_destroy(&mutex);

	return 0;
}

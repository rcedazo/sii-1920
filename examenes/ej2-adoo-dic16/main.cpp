#include <iostream>
#include <string>
//LibraryOne
class LibraryOne
{

public:
    void ThisIsHowOneDoesIt()
    {
        std::cout<< "Using Library ONE to perform the action\n";
    }
};

//LibraryTwo
class LibraryTwo
{
public:
    std::string ThisIsHowTwoDoesIt()
    {
        return "Using Library TWO to perform the action\n";
    }
};

class IAdapter
{
public:
    virtual void Do() = 0;
};

class AdapterOne : public IAdapter
{
public:
    AdapterOne() {}
    void Do()
    {
        LibraryOne one;
        one.ThisIsHowOneDoesIt();
    }
};
class AdapterTwo : public IAdapter
{
public:
    AdapterTwo() {}
    void Do()
    {
        LibraryTwo two;
        std::cout << two.ThisIsHowTwoDoesIt();
    }
};

int main(int argc, char* argv[])
{
    IAdapter *adapter = 0;
    std::cout << "Enter which library you wanna use to do operation";
    std::cout << " {1,2}: ";
    int x;
    std::cin >> x;
    if (x == 1)
    {
        adapter = new AdapterOne();
    }
    else if (x == 2)
    {
        adapter = new AdapterTwo();
    }
    if(adapter)
    {
        adapter->Do();
        delete adapter;
    }
    return 0;
};

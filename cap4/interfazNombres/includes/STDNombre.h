// De: "Apuntes de Sistemas Informáticos Industriales" Carlos Platero.
// (R) 2013 Con licencia GPL.
// Ver permisos en licencia de GPL



#ifndef _INC_STDNOMBRE_
#define _INC_STDNOMBRE_

#include <string>
#include "INombre.h"

class STDNombre : public INombre 
{
public:
	friend class INombre;
	 virtual void setNombre(const char *cadena)
	 { elNombre = cadena; }
	 virtual const char * getNombre (void)
	 { return (elNombre.c_str());}

private:
	std::string elNombre;
	STDNombre(){}	
};

#endif


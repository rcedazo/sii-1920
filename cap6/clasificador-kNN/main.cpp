#include <vector>
#include <iostream>
using namespace std;

class Muestra
{
    vector<float> vector_muestra;
    int etiqueta;
public:
    Muestra(float *pV,unsigned dim,int et=-1): etiqueta(et)
    {
        for(unsigned i=0; i<dim; i++)
            vector_muestra.push_back(pV[i]);
    }
    int getEtiqueta()
    {
        return etiqueta;
    }
    float getVector(int i)
    {
        return vector_muestra[i];
    }
};

class BaseEntrenamiento
{
    vector<Muestra *> listaMuestras;
    unsigned dim_vector;
public:
    BaseEntrenamiento(unsigned dim):dim_vector(dim) {}
    void CargarMuestra(float *pVector,int etiqueta)
    {
        listaMuestras.push_back(new Muestra(pVector,dim_vector,etiqueta));
    }
    Muestra*getMuestra(unsigned i)
    {
        return listaMuestras[i];
    }
    float getVector(int i, int j)
    {
        return listaMuestras[i]->getVector(j);
    }
    int getEtiqueta(unsigned i)
    {
        return  listaMuestras[i]->getEtiqueta();
    }
    unsigned getNumMuestras()
    {
        return listaMuestras.size();
    }
    unsigned getDimension()
    {
        return dim_vector;
    }
    ~BaseEntrenamiento()
    {
        for(unsigned i=0; i<listaMuestras.size(); i++)
            delete listaMuestras[i];
    }
};

typedef enum {kNN,Bayes,RandomForest} tipoClasificador;

class Clasificador
{
public:
    virtual int getEtiqueta(Muestra *) = 0;
};
float distancia_muestras(Muestra *pM1, Muestra *pM2,
                         unsigned dim)
{
    float resultado=0.0f;
    for(unsigned i=0; i< dim; i++)
        resultado += (pM1->getVector(i)-pM2->getVector(i))*
                     (pM1->getVector(i)-pM2->getVector(i));
    return resultado;
}
class kVecinos: public Clasificador
{
    friend class Factoria;
    BaseEntrenamiento *pBaseDatos;
    kVecinos(BaseEntrenamiento *pBase):pBaseDatos(pBase) {}
public:
    virtual int getEtiqueta(Muestra *pMuestra)
    {
        int etiqueta = -1;
        unsigned numMuestras = pBaseDatos->getNumMuestras();
        unsigned dim = pBaseDatos->getDimension();
        float min_distancia = 1e10;
        for(unsigned i=0; i<numMuestras; i++)
        {
            float dist = distancia_muestras
                         (pMuestra,pBaseDatos->getMuestra(i),dim);
            if(dist < min_distancia)
            {
                min_distancia = dist;
                etiqueta= pBaseDatos->getEtiqueta(i);
            }
        }
        return etiqueta;
    }
};

class Factoria
{
    Factoria() {}
public:
    static Factoria& getInstancia()
    {
        static Factoria unicaInstancia;
        return unicaInstancia;
    }
    Clasificador* crearClasificador(tipoClasificador tipo,
                                    BaseEntrenamiento *pBase )
    {
        if(tipo == kNN) return new kVecinos(pBase);
        else return NULL;
    }

};

int main()
{
    unsigned dim = 2;
    unsigned numMuestras = 5;
    float vectores_muestra[5][2] = { {1.0f,1.0f},{0.0f,0.0f},
        {2.0f,2.0f}, {2.0f,3.0f}, {3.0f,2.0f}
    };
    int etiquetas_muestra[] = {1,1,2,2,2};
    BaseEntrenamiento laBase(dim);

    for(unsigned i=0; i<numMuestras; i++)
        laBase.CargarMuestra
        (vectores_muestra[i],etiquetas_muestra[i]);

    float vector_nuevo[]= {1.0,2.0};
    Muestra laMuestra(vector_nuevo,dim);

// Clasificar la muestra
    Factoria laFactoria = Factoria::getInstancia();
    Clasificador *pClasificador =
        laFactoria.crearClasificador(kNN,&laBase);
    cout <<"La muestra con vector: "
         << laMuestra.getVector(0)<<" "
     << laMuestra.getVector(1) << " tiene la etiqueta: "
         << pClasificador->getEtiqueta(&laMuestra) << endl;
         if(pClasificador) delete pClasificador;

         return 0;
     }

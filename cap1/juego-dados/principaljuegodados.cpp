#include <iostream>
#include <cstdio>
#include "JuegoDados.h"


int main ()
{
	bool ganarPartida, continuarJuego = true;
	char tecla;
	JuegoDados jugador1;

	std::cout << "\nJuego de dados, lanzar dos dados y sumar 7 para ganar\n";


	while (continuarJuego)
	{
		std::cout << "\nPulsar cualquier tecla para lanzar dados\n";
		std::getchar();

		ganarPartida = jugador1.jugar();

		if ( ganarPartida )
			std:: cout << "Ha ganando.\n";
		else
			std:: cout << "Ha perdido.\n";

		std::cout<<"\nSi desea jugar otra vez, pulse C\n";
		std::cin>> tecla;
		continuarJuego = (tecla == 'c')||(tecla == 'C')?true:false;
	}

	std::cout << "Se termino el juego de dados. Un saludo\n";
	return 0;
}






